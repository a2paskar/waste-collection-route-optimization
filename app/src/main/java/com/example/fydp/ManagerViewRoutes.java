package com.example.fydp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ManagerViewRoutes extends FragmentActivity implements OnMapReadyCallback, VolleyListener {
    private GoogleMap mMap;
    private Map<Integer, String> districtMap = loadingScreen.districtMap;
    private ArrayList<Manager> managerArrayList = loadingScreen.managerArrayList;
    private Map<Integer, Driver> driverMap = loadingScreen.driverMap;
    private User currentUser = ManagerLogin.currentUser;
    private District districtToBeAssigned;
    private Driver driverToBeAssigned;
    private Route routeToBeAssigned;
    private Map<Integer, Bin> binMap = loadingScreen.binMap;
    private ArrayList<Route> routeArrayList = loadingScreen.routeArrayList;
    private String apiKey = "AIzaSyAB9VxjsGpKlHRMSp8yJMwcMXzqA90Rtuw";
    private List<LatLng> coordinates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_view_routes);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map4);
        mapFragment.getMapAsync(this);

        createAndPopulateDistrictsSpinner();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    private void createAndPopulateDistrictsSpinner() {
        Spinner districtsSpinner = findViewById(R.id.DistrictsSpinner2);

        ArrayList<String> districtsSpinnerArray = new ArrayList<String>();

        for(Map.Entry<Integer, String> entry : districtMap.entrySet()) {
            districtsSpinnerArray.add(entry.getValue() + "\nID:"+ entry.getKey());
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_dropdown_item,
                districtsSpinnerArray);

        districtsSpinner.setAdapter(spinnerArrayAdapter);
        districtsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String district =(String)parent.getSelectedItem();
                getDistrictToAssign(district);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                return;
            }
        });
    }

    private void getDistrictToAssign(String districtAndId) {
        int idStartIndex = districtAndId.indexOf(":");
        int districtId = Integer.parseInt(
                districtAndId.substring(idStartIndex+1)
        );
        districtToBeAssigned = new District(districtId, districtMap.get(districtId));

        // We  wanna get the route associated with this district (one to one).
        // The route will give us bin IDs and the we wanna move the camera to the first
        // LatLng of the first bin
        for(Route route: routeArrayList) {
            if(route.getDistrictId() == districtToBeAssigned.getId()) {
                routeToBeAssigned = route;
                break;
            }
        }
        // Get the first bin and move camera to it
        Bin binToMoveTo = binMap.get(routeToBeAssigned.getOrder().get(0));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(binToMoveTo.getCoords(), 15));

        List<LatLng> locationsList = new ArrayList<>();

        //First remove all markers and polylines
        mMap.clear();

        //Place markers based on the selected route
        for(int i=0; i<routeToBeAssigned.getOrder().size(); i++) {
            Bin binToMark = binMap.get(routeToBeAssigned.getOrder().get(i));
            locationsList.add(binToMark.getCoords());
            mMap.addMarker(new MarkerOptions()
                    .position(binToMark.getCoords())
                    .title("Capacity: " + binToMark.getCapacity() + "%"));
        }

        //Add the polylines
        // Make volley request to the Directions API
        for(int i=0; i<=(locationsList.size()-2); i++) {
            getDirections(locationsList.get(i), locationsList.get(i+1));
        }
    }

    public void getDirections(LatLng origin, LatLng destination) {
        // Instantiate VolleyListener
        final VolleyListener volleyListener = (VolleyListener)this;
        // Instantiate the RequestQueue
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + origin.latitude + "," + origin.longitude +
                "&destination=" + destination.latitude + "," + destination.longitude + "&key=" +
                apiKey;
        // Request a string response from the provided URL
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            volleyListener.directionsApiRequestFinished(true, jsonResponse, null, null);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyListener.directionsApiRequestFinished(false, null, null, null);
            }
        });

        // Add the request to the RequestQueue
        queue.add(stringRequest);
    }

    public void drawPolyline(List<LatLng> coordinates) {
        PolylineOptions pOptions = new PolylineOptions().clickable(true);
        for (LatLng location: coordinates) {
            pOptions.add(location);
        }
        mMap.addPolyline(pOptions);
    }

    @Override
    public void directionsApiRequestFinished(boolean responseArrived, JSONObject response, Bin binToUpdate, Bin destinationBin) {
        //Draw the polyline
        if(responseArrived) {
            try {
                JSONArray routesArray = response.getJSONArray("routes");
                JSONObject firstRoute = routesArray.getJSONObject(0);
                JSONObject polylines = firstRoute.getJSONObject("overview_polyline");
                String points = polylines.getString("points");
                coordinates = PolyUtil.decode(points);
                drawPolyline(coordinates);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void distanceApiRequestFinished(boolean responseArrived, JSONObject response, Bin binToUpdate, Bin destinationBin) {

    }

}
