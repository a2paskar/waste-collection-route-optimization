package com.example.fydp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.TextViewCompat;;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class DriverViewRoutes extends AppCompatActivity {

    public static User currentUser = DriverLogin.currentUser;
    public static List<Route> userRoutes = new ArrayList<>();
    public static Route selectedRoute;
    private Integer textViewIdCounter = 1;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_view_routes);

        //the layout on which you are working
        LinearLayout layout = findViewById(R.id.linearLayout);

        //Adds a textview with some instructional text
        addInformationTextView(layout);

        //Get all of the current user's incomplete routes
        for(Route route: loadingScreen.routeArrayList) {
            if(route.getDriverId() == currentUser.getId() && route.getStatus().equals("incomplete")) {
                if(userRoutes.indexOf(route) == -1) userRoutes.add(route);
            }
        }

        userRoutes.removeIf(r -> (r.getStatus().equals("complete")));

        //Add a textview for each route
        for(Route route: userRoutes) {
            addRouteTextView(layout, route);
        }
    }

    private void addRouteTextView(LinearLayout layout, Route route) {
        TextView routeTextView = new TextView(this);
        routeTextView.setText("Route Location: " + loadingScreen.districtMap.get(route.getDistrictId())
                + "\nRoute ID: " + route.getId());
        routeTextView.setPaintFlags(routeTextView.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        TextViewCompat.setTextAppearance(routeTextView, android.R.style.TextAppearance_DeviceDefault_Large);
        routeTextView.setId(textViewIdCounter);
        textViewIdCounter++;
        routeTextView.setGravity(Gravity.CENTER);
        routeTextView.setPadding(0,20,0,20);
        layout.addView(routeTextView);

        routeTextView.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent i = new Intent(DriverViewRoutes.this, DriverRouteMap.class);
                selectedRoute = route;
                startActivity(i);
            }
        });
    }

    private void addInformationTextView(LinearLayout layout) {
        //set the properties for button
        TextView informationText = new TextView(this);
        //btnTag.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT));
        informationText.setText("All of your incomplete routes are listed below. To view the routes, simply press on the desired route.");
        TextViewCompat.setTextAppearance(informationText, android.R.style.TextAppearance_DeviceDefault_Large);
        informationText.setId(Integer.parseInt("1"));
        informationText.setGravity(Gravity.CENTER);
        informationText.setPadding(40,50,40,40);
        layout.addView(informationText);
    }
}
