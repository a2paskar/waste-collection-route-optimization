package com.example.fydp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.ui.IconGenerator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DriverRouteMap extends FragmentActivity implements OnMapReadyCallback, VolleyListener {
    private GoogleMap mMap;
    private List<LatLng> coordinates;
    private String apiKey = "AIzaSyAB9VxjsGpKlHRMSp8yJMwcMXzqA90Rtuw";
    private List<LatLng> locationsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_route_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map2);
        mapFragment.getMapAsync(this);

        Button startGoogleMapsBtn = findViewById(R.id.startNavigatingBtn);

        startGoogleMapsBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                openGoogleMaps(locationsList);
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        IconGenerator iconFactory = new IconGenerator(this);

        Route selectedRoute = DriverViewRoutes.selectedRoute;
        boolean cameraMoved = false;

        //Add markers
        for(Integer binId: selectedRoute.getOrder()) {
            Bin bin = loadingScreen.binMap.get(binId);
            LatLng location = bin.getCoords();
            locationsList.add(location);
            mMap.addMarker(new MarkerOptions()
                    .position(location)
                    .title("Capacity: " + bin.getCapacity() + "%"));
            //Move the camera to the first bin
            if(!cameraMoved) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 17));
                cameraMoved = true;
            }
        }
        // Make volley request to the Directions API
        for(int i=0; i<=(locationsList.size()-2); i++) {
            getDirections(locationsList.get(i), locationsList.get(i+1));
        }

    }

    public void openGoogleMaps(List<LatLng> locations) {
        LatLng startLocation = locations.get(0);
        LatLng destination = locations.get(locations.size()-1);

        //Add in the start and destination coordinates first
        String apiString = "https://www.google.com/maps/dir/?api=1&origin=" +
                startLocation.latitude + "," + startLocation.longitude + "&destination=" +
                destination.latitude + "," + destination.longitude + "&waypoints=";

        //loop through locations list starting at index 1 and add to apiString
        for(int i=1; i<=locations.size()-2; i++) {
            apiString += locations.get(i).latitude + "," + locations.get(i).longitude + "|";
        }

        apiString += "&travelmode=driving";
        Uri gmmIntentUri = Uri.parse(apiString);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    public void getDirections(LatLng origin, LatLng destination) {
        // Instantiate VolleyListener
        final VolleyListener volleyListener = (VolleyListener)this;
        // Instantiate the RequestQueue
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + origin.latitude + "," + origin.longitude +
                "&destination=" + destination.latitude + "," + destination.longitude + "&key=" +
                apiKey;
        // Request a string response from the provided URL
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            volleyListener.directionsApiRequestFinished(true, jsonResponse, null, null);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyListener.directionsApiRequestFinished(false, null, null, null);
            }
        });

        // Add the request to the RequestQueue
        queue.add(stringRequest);
    }

    public void drawPolyline(List<LatLng> coordinates) {
        PolylineOptions pOptions = new PolylineOptions().clickable(true);
        for (LatLng location: coordinates) {
            pOptions.add(location);
        }
        mMap.addPolyline(pOptions);
    }

    @Override
    public void directionsApiRequestFinished(boolean responseArrived, JSONObject response, Bin binToUpdate, Bin destinationBin) {
        //Draw the polyline
        if(responseArrived) {
            try {
                JSONArray routesArray = response.getJSONArray("routes");
                JSONObject firstRoute = routesArray.getJSONObject(0);
                JSONObject polylines = firstRoute.getJSONObject("overview_polyline");
                String points = polylines.getString("points");
                coordinates = PolyUtil.decode(points);
                drawPolyline(coordinates);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void distanceApiRequestFinished(boolean responseArrived, JSONObject response, Bin binToUpdate, Bin destinationBin) {

    }
}
