package com.example.fydp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.fragment.app.FragmentActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.ui.IconGenerator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, VolleyListener {
    private GoogleMap mMap;
    private List<LatLng> coordinates;
    private String apiKey = "AIzaSyAB9VxjsGpKlHRMSp8yJMwcMXzqA90Rtuw";

    //holds list of our Bin objects
    private ArrayList<Bin> binArrayList = new ArrayList<Bin>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.districts, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        Button generateRoute = (Button)findViewById(R.id.generateRouteBtn);

        generateRoute.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                openGoogleMaps(v); //idk lol
                //startActivity(new Intent(SelectDistrict.this, MapsActivity.class));
            }
        });

        normalizeBinFeatures(binArrayList);
        findOptimalPath(binArrayList);
    }

    private void findOptimalPath(ArrayList<Bin> binArrayList) {

    }

    private void normalizeBinFeatures(ArrayList<Bin> binArrayList) {
    }

    private ArrayList<ArrayList<Double>> featureScale(ArrayList<Double> cap, ArrayList<Double> dist,
                                                      ArrayList<Double> num_L,
                                                      ArrayList<Double> times) {
        Double max_cap = Collections.max(cap);
        Double min_cap = Collections.min(cap);
        Double max_dist = Collections.max(dist);
        Double min_dist = Collections.min(dist);
        Double max_left = Collections.max(num_L);
        Double min_left = Collections.min(num_L);
        Double max_time = Collections.max(times);
        Double min_time = Collections.min(times);

        for (int i = 0; i < cap.size(); i++) {
            cap.set(i, normalize(cap.get(i), max_cap, min_cap));
            dist.set(i, normalize(dist.get(i), max_dist, min_dist));
            num_L.set(i, normalize(num_L.get(i), max_left, min_left));
            times.set(i, normalize(times.get(i), max_time, min_time));
        }

        ArrayList<ArrayList<Double>> binFeatures = new ArrayList<ArrayList<Double>>();
        //loop over list of Bin objects
        for (int i = 0; i < binArrayList.size(); i++) {
            //reset binAccum for each iteration- will hold rows of binFeatures matrix
            ArrayList<Double> binAccum = new ArrayList<Double>();
            binAccum.add(cap.get(i));
            binAccum.add(dist.get(i));
            binAccum.add(num_L.get(i));
            binAccum.add(times.get(i));
            binFeatures.add(binAccum);
        }
        return binFeatures;
    }

    private ArrayList<Double> matrixRowSum(ArrayList<ArrayList<Double>> binFeatures) {
        ArrayList<Double> sumo = new ArrayList<Double>();
        Double sum;
        for (int i = 0; i < binFeatures.size(); i++) {
            sum = 0.0;
            for (int j = 0; j < binFeatures.get(0).size(); j++) {
                sum = sum + binFeatures.get(i).get(j);
            }
            sumo.add(sum);
        }
        return sumo;
    }

    private Double normalize(Double x, Double max, Double min) {
        return ((x - min) / (max - min));
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        IconGenerator iconFactory = new IconGenerator(this);

        // Add a marker in Toronto and move the camera
        LatLng toronto = new LatLng(43.65, -79.38);
        mMap.addMarker(new MarkerOptions().position(toronto).title("Marker in Toronto"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(toronto, 17));

        // TODO: Write a method that adds these markers at all the required locations. Maybe include capacity in the title?
        LatLng loc1 = new LatLng(43.651184, -79.378910);
        Marker marker1 = mMap.addMarker(new MarkerOptions()
                .position(loc1));
                //.title("Location #1")
                //.snippet("Capacity: 100%"));

        //marker1.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.blue_icon_background));
        //marker1.setTitle("Location #1");
        //marker1.showInfoWindow();
        LatLng loc2 = new LatLng(43.651156, -79.380859);
        Marker marker2 = mMap.addMarker(new MarkerOptions()
                .position(loc2)
                .title("Location #2")
                .snippet("Capacity: 50%"));
        //marker2.setIcon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon("Location #2")));
        marker2.showInfoWindow();
        mMap.addMarker(new MarkerOptions().position(loc1).title("Location #1"));
//        LatLng loc2 = new LatLng(43.651156, -79.380859);
//        mMap.addMarker(new MarkerOptions().position(loc2).title("Location #2"));
        LatLng loc3 = new LatLng(43.650442, -79.381277);
        mMap.addMarker(new MarkerOptions().position(loc3).title("Location #3"));
        LatLng loc4 = new LatLng(43.649604, -79.380939);
        mMap.addMarker(new MarkerOptions().position(loc4).title("Location #4"));

        // Make volley request to the Directions API
        getDirections(loc1, loc2);
        getDirections(loc2, loc3);
        getDirections(loc3, loc4);
        // Open up Google Maps using intent
        //TODO: Write a method that constructs the uri string for any number of locations

    }

    public void openGoogleMaps(View view) {
        LatLng loc1 = new LatLng(43.651184, -79.378910);
        LatLng loc2 = new LatLng(43.651156, -79.380859);
        LatLng loc3 = new LatLng(43.650442, -79.381277);
        LatLng loc4 = new LatLng(43.649604, -79.380939);
        // getDirections(loc1, loc2);
        // Open up Google Maps using intent
        //TODO: Write a method that constructs this string for any number of locations
        //TODO: Add a button on top of the Maps fragment that executes this intent when clicked
        Uri gmmIntentUri = Uri.parse("https://www.google.com/maps/dir/?api=1&origin=" +
                loc1.latitude + "," + loc1.longitude + "&destination=" + loc4.latitude + "," +
                loc4.longitude + "&waypoints=" + loc2.latitude + "," + loc2.longitude + "|" +
                loc3.latitude + "," + loc3.longitude + "&travelmode=driving");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    public void getDirections(LatLng origin, LatLng destination) {
        // Instantiate VolleyListener
        final VolleyListener volleyListener = (VolleyListener)this;
        // Instantiate the RequestQueue
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + origin.latitude + "," + origin.longitude +
                "&destination=" + destination.latitude + "," + destination.longitude + "&key=" +
                apiKey;
        // Request a string response from the provided URL
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    volleyListener.directionsApiRequestFinished(true, jsonResponse, null, null);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyListener.directionsApiRequestFinished(false, null, null, null);
            }
        });

        // Add the request to the RequestQueue
        queue.add(stringRequest);
    }

    public void drawPolyline(List<LatLng> coordinates) {
        PolylineOptions pOptions = new PolylineOptions().clickable(true);
        for (LatLng location: coordinates) {
            pOptions.add(location);
        }
        mMap.addPolyline(pOptions);
    }

    @Override
    public void directionsApiRequestFinished(boolean responseArrived, JSONObject response, Bin binToUpdate, Bin destinationBin) {
        //Draw the polyline
        if(responseArrived) {
            try {
                JSONArray routesArray = response.getJSONArray("routes");
                JSONObject firstRoute = routesArray.getJSONObject(0);
                JSONObject polylines = firstRoute.getJSONObject("overview_polyline");
                String points = polylines.getString("points");
                coordinates = PolyUtil.decode(points);
                drawPolyline(coordinates);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void distanceApiRequestFinished(boolean responseArrived, JSONObject response, Bin binToUpdate, Bin destinationBin) {

    }
}


