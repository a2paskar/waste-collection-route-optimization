package com.example.fydp;

import java.util.List;

public class Route {
    private Integer id;
    private Integer districtId;
    private Integer managerId;
    private Integer driverId;
    private String status;
    private List<Integer> order;

    public Route(Integer id, Integer districtId, Integer managerId, Integer driverId, String status, List<Integer> order) {
        this.id = id;
        this.districtId = districtId;
        this.managerId = managerId;
        this.driverId = driverId;
        this.status = status;
        this.order = order;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public Integer getManagerId() {
        return managerId;
    }

    public void setManagerId(Integer managerId) {
        this.managerId = managerId;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Integer> getOrder() {
        return order;
    }

    public void setOrder(List<Integer> order) {
        this.order = order;
    }
}
