package com.example.fydp;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.geometry.Point;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class loadingScreen extends AppCompatActivity implements VolleyListener {
    //holds list of our Bin objects
    public static ArrayList<Bin> binArrayList = new ArrayList<>();
    public static ArrayList<Bin> previousTravel = new ArrayList<>();

    public static Map<Integer, String> districtMap = new HashMap<>();
    public static Map<Integer, Bin> binMap = new HashMap<>();
    public static Map<Integer, Driver> driverMap = new HashMap<>();
    public static ArrayList<Driver> driverArrayList = new ArrayList<>();
    public static ArrayList<Manager> managerArrayList = new ArrayList<>();
    public static ArrayList<Route> routeArrayList = new ArrayList<>();
    //Holds chronological list of locations (LatLng objects) to go to
    private ArrayList<LatLng> locations = new ArrayList<LatLng>();
    private boolean isLoadingComplete = false;
    private String apiKey = "AIzaSyAB9VxjsGpKlHRMSp8yJMwcMXzqA90Rtuw";
    private LatLng startLocation = new LatLng(43.651200, -79.3878911);
    private int numOfApiCallsComplete = 0;
    private int maxNumOfApiCalls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading_screen);

        connectToDB();
        int i = 0;

        // Get all bins in the DB and add to binsArrayList

        //dummy data for testing
//        LatLng loc1 = new LatLng(43.651184, -79.378910);
//        LatLng loc2 = new LatLng(43.651156, -79.380859);
//        LatLng loc3 = new LatLng(43.650442, -79.381277);
//        LatLng loc4 = new LatLng(43.649604, -79.380939);
//        Bin bin01 = new Bin(0.69, loc1.latitude, loc1.longitude);
//        Bin bin02 = new Bin(0.94, loc2.latitude, loc2.longitude);
//        Bin bin03 = new Bin(0.77, loc3.latitude, loc3.longitude);
//        Bin bin04 = new Bin(0.55, loc4.latitude, loc4.longitude);
//        binArrayList.add(bin01);
//        binArrayList.add(bin02);
//        binArrayList.add(bin03);
//        binArrayList.add(bin04);

        getDistancesAndTurns();
        double startingTemperature= 60.0;
        int numberOfIterations= 50;
        double coolingRate= 0.01;

        simulateAnnealing(startingTemperature,
         numberOfIterations, coolingRate);

    }

    private void getDistancesAndTurns() {
        int numOfBins = binArrayList.size();
        maxNumOfApiCalls = (binArrayList.size() * (binArrayList.size() - 1)) * 2;
        int numOfApiCalls = 0;

        for (int i = 0; i < binArrayList.size(); i++) {
            for(int x = 0; x < binArrayList.size(); x++) {
                // we cant make origin and destination equal the same bin
                if (x == i ) continue;
                if (numOfApiCalls == (maxNumOfApiCalls - 1)) {
                }

                //Get distance and update the bin object to hold the distances
                distanceMatrixApiCall(binArrayList.get(i), binArrayList.get(x));
                directionsApiCall(binArrayList.get(i), binArrayList.get(x));
                numOfApiCalls++;
            }
        }
    }

    private void directionsApiCall(Bin originBin, Bin destinationBin) {
        LatLng origin = originBin.getCoords();
        LatLng destination = destinationBin.getCoords();
        // Instantiate VolleyListener
        final VolleyListener volleyListener = (VolleyListener)this;
        // Instantiate the RequestQueue
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + origin.latitude
                + "," + origin.longitude + "&destination=" + destination.latitude + ","
                + destination.longitude + "&key=" + apiKey;
        // Request a string response from the provided URL
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            volleyListener.directionsApiRequestFinished(true, jsonResponse, originBin, destinationBin);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyListener.directionsApiRequestFinished(false, null, null, null);
            }
        });
        // Add the request to the RequestQueue
        queue.add(stringRequest);
    }

    @Override
    public void directionsApiRequestFinished(boolean responseArrived, JSONObject response, Bin originBin, Bin destinationBin) {
        try {
            JSONArray routesArray = response.getJSONArray("routes");
            JSONObject firstRoute = routesArray.getJSONObject(0);
            JSONObject polylines = firstRoute.getJSONObject("overview_polyline");
            String points = polylines.getString("points");
            List<LatLng> coordinates = PolyUtil.decode(points);
            // Run this list of LatLngs through left turn counter function
            int numLeftTurns = calculateLeftTurns(coordinates);

            //Set the distance and time from origin -> destination bin
            for(int i = 0; i < binArrayList.size(); i++) {
                if(binArrayList.get(i).getCoords() == originBin.getCoords()){
                    binArrayList.get(i).setLeftTurnsToBin(destinationBin, numLeftTurns);
                    break;
                }
            }
            numOfApiCallsComplete++;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(numOfApiCallsComplete == maxNumOfApiCalls) startMapsActivity(binArrayList);
    }

    private int calculateLeftTurns(List<LatLng> coordinatesList) {
        //Convert LatLng to x,y
        ArrayList<Point> listOfPoints = new ArrayList<>();
        for(LatLng coordinate: coordinatesList) {
            Point point = new Point(MercatorProjection.lon2x(coordinate.longitude),
                    MercatorProjection.lat2y(coordinate.latitude));
            listOfPoints.add(point);
        }
        int leftTurnCounter = 0;
        for(int i = 2; i < listOfPoints.size(); i++) {
            Point pointA = listOfPoints.get(i-2);
            Point pointB = listOfPoints.get(i-1);
            Point pointC = listOfPoints.get(i);

            if(getDirection(pointA,pointB,pointC)) leftTurnCounter++;
        }
        return leftTurnCounter;
    }

    private Boolean getDirection(Point a, Point b, Point c)
    {
        double theta1 = GetAngle(a, b);
        double theta2 = GetAngle(b, c);
        double delta = NormalizeAngle(theta2 - theta1);

        if ( delta == 0 )
            return false;//straight
        else if ( delta == Math.PI )
            return false;//backwards
        else if ( delta < Math.PI && delta >= Math.toRadians(60))
            return true; //left
        else return false; //right
    }

    private Double GetAngle(Point p1, Point p2)
    {
        Double angleFromXAxis = Math.atan ((p2.y - p1.y ) / (p2.x - p1.x) ); // where y = m * x + K
        return  p2.x - p1.x < 0 ? angleFromXAxis + Math.PI : angleFromXAxis; // The will go to the correct Quadrant
    }

    private Double NormalizeAngle(Double angle)
    {
        return angle < 0 ? angle + 2 * Math.PI : angle; //This will make sure angle is [0..2PI]
    }


    private void distanceMatrixApiCall(Bin originBin, Bin destinationBin) {
        LatLng origin = originBin.getCoords();
        LatLng destination = destinationBin.getCoords();
        String apiUrl = "https://maps.googleapis.com/maps/api/distancematrix/json?";
        String originParameter = "origins=" + origin.latitude + "," + origin.longitude;
        String destinationParameter ="&destinations=" + destination.latitude + "," + destination.longitude;
        String keyParameter = "&key=" + apiKey;
        //instantiate volley listener
        final VolleyListener volleyListener = (VolleyListener) this;
        // Instantiate the RequestQueue
        RequestQueue queue = Volley.newRequestQueue(this);

        String url = apiUrl + originParameter + destinationParameter + keyParameter;
        // Request a string response from the provided URL
        StringRequest distanceApiRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    volleyListener.distanceApiRequestFinished(true, jsonResponse, originBin, destinationBin);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyListener.distanceApiRequestFinished(false, null, null, null);
            }
        });
        // Add the request to the RequestQueue
        queue.add(distanceApiRequest);
    }

    @Override
    public void distanceApiRequestFinished(boolean responseArrived, JSONObject response, Bin originBin, Bin destinationBin) {
        try {
            //Extract the distance from the API response
            JSONArray rowsArray = response.getJSONArray("rows");
            JSONObject firstRow = rowsArray.getJSONObject(0);
            JSONArray elementsArray = firstRow.getJSONArray("elements");
            JSONObject firstElement = elementsArray.getJSONObject(0);
            JSONObject distancesObject = firstElement.getJSONObject("distance");
            JSONObject durationObject = firstElement.getJSONObject("duration");
            Double timeInSeconds = durationObject.getDouble("value");
            Double distanceInMeters = distancesObject.getDouble("value");

            //Set the distance and time from origin -> destination bin
            for(int i = 0; i < binArrayList.size(); i++) {
                if(binArrayList.get(i).getCoords() == originBin.getCoords()){
                    binArrayList.get(i).setDistanceTo(destinationBin, distanceInMeters);
                    binArrayList.get(i).setTimeTo(destinationBin, timeInSeconds);
                    break;
                }
            }
            numOfApiCallsComplete++;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(numOfApiCallsComplete == maxNumOfApiCalls) startMapsActivity(binArrayList);
    }

    private void startMapsActivity(ArrayList<Bin> binArrayList) {
        Intent i = new Intent(loadingScreen.this, IdentifyUser.class);
        startActivity(i);
    }

    private void getBins(DatabaseReference ref) {
        //points to Node Bins ~ look at firebase console to see data structure
        DatabaseReference myBin = ref.child("bin");
        // Read from the database
        myBin.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                // binArrayList.removeAll(binArrayList);

                for (DataSnapshot d : ds.getChildren()) {

                    //Make sure data gets converted correctly
                    Bin bin = new Bin(Double.valueOf(d.child("capacity").getValue().toString()),
                            Double.valueOf(d.child("latitude").getValue().toString()),
                            Double.valueOf(d.child("longitude").getValue().toString()),
                            Integer.valueOf(d.child("id").getValue().toString()),
                            Integer.valueOf(d.child("district_id").getValue().toString()));
                    binArrayList.add(bin);
                    binMap.put(bin.getId(), bin);
                }

                getDistricts(ref);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });
    }

    private void getDrivers(DatabaseReference ref) {
        DatabaseReference myBin = ref.child("driver");
        myBin.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {
                for (DataSnapshot d : ds.getChildren()) {
                    Driver driver = new Driver(Integer.valueOf(d.child("id").getValue().toString()),
                            Integer.valueOf(d.child("managerId").getValue().toString()),
                            d.child("name").getValue().toString());
                    driverArrayList.add(driver);
                    driverMap.put(driver.getId(), driver);
                }

                getManagers(ref);
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });
    }

    private void getDistricts(DatabaseReference ref) {
        //points to Node Bins ~ look at firebase console to see data structure
        DatabaseReference myBin = ref.child("district");
        // Read from the database
        myBin.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                // binArrayList.removeAll(binArrayList);

                for (DataSnapshot d : ds.getChildren()) {

                    //Make sure data gets converted correctly
                    District district = new District(Integer.valueOf(d.child("id").getValue().toString()),
                            d.child("name").getValue().toString());
                    districtMap.put(district.getId(), district.getName());
                }

                getDrivers(ref);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });
    }

    private void getManagers(DatabaseReference ref) {
        DatabaseReference myBin = ref.child("manager");
        List<Integer> driversIdList = new ArrayList<>();
        myBin.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {
                for (DataSnapshot d : ds.getChildren()) {
                    DataSnapshot drivers = d.child("drivers");
                    for(DataSnapshot driverIds: drivers.getChildren()) {
                        driversIdList.add(Integer.valueOf(driverIds.getValue().toString()));
                    }
                    Manager manager = new Manager(
                            Integer.valueOf(d.child("id").getValue().toString()),
                            d.child("name").getValue().toString(),
                            driversIdList
                            );
                    managerArrayList.add(manager);
                }

                getRoutes(ref);
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });
    }

    private void getRoutes(DatabaseReference ref) {
        DatabaseReference myBin = ref.child("route");
        myBin.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {
                for (DataSnapshot d : ds.getChildren()) {
                    List<Integer> orderedBinIds = new ArrayList<>();
                    DataSnapshot order = d.child("order");
                    for(DataSnapshot binIds: order.getChildren()) {
                        orderedBinIds.add(Integer.valueOf(binIds.getValue().toString()));
                    }
                    Route route = new Route(
                            Integer.valueOf(d.child("id").getValue().toString()),
                            Integer.valueOf(d.child("districtId").getValue().toString()),
                            Integer.valueOf(d.child("managerId").getValue().toString()),
                            Integer.valueOf(d.child("driverId").getValue().toString()),
                            d.child("status").getValue().toString(),
                            orderedBinIds
                            );
                    routeArrayList.add(route);
                }

                getDistancesAndTurns();
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });
    }

    public void swapBins(){

        Random r = new Random();
        r.nextInt((binArrayList.size()) + 1);
        int a = r.nextInt((binArrayList.size()) + 1);
        Random n= new Random();
        int b = n.nextInt((binArrayList.size()) + 1);
        previousTravel = binArrayList;

        Bin x = binArrayList.get(a);
        Bin y = binArrayList.get(b);
        binArrayList.set(a, y);
        binArrayList.set(b, x);
    }

    public void revertSwap(){
        binArrayList= previousTravel;
    }

    public double getTotalCost() {
        int cost = 0;
        for (int index = 0; index < binArrayList.size(); index++) {
            Bin starting = binArrayList.get(index);
            Bin destination;
            if (index + 1 < binArrayList.size()) {
                destination = binArrayList.get(index + 1);
            } else {
                destination = binArrayList.get(0);
            }
            cost += starting.getCostTo(destination);
        }
        return cost;
    }

    public void simulateAnnealing(double startingTemperature,
                                    int numberOfIterations, double coolingRate) {
        double t = startingTemperature;
        double bestCost = getTotalCost();

        for (int i = 0; i < numberOfIterations; i++) {
            if (t > 0.1) {
                swapBins();
                double currentCost = getTotalCost();
                if (currentCost < bestCost) {
                    bestCost = currentCost;
                } else if (Math.exp((bestCost - currentCost) / t) < Math.random()) {
                    revertSwap();
                }
            } else {
                continue;
            }
            t *= coolingRate;
        }
    }


    private void connectToDB() {
        //establish connection to realtime database and get reference to it
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference();

        getBins(myRef);
    };







}
