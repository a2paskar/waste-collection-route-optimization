package com.example.fydp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.maps.android.ui.IconGenerator;

import java.util.ArrayList;
import java.util.Map;

public class ManagerAssignRoute extends FragmentActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    // Add a marker in Toronto and move the camera
    private Map<Integer, String> districtMap = loadingScreen.districtMap;
    private ArrayList<Manager> managerArrayList = loadingScreen.managerArrayList;
    private Map<Integer, Driver> driverMap = loadingScreen.driverMap;
    private District districtToBeAssigned;
    private Driver driverToBeAssigned;
    private Route routeToBeAssigned;

    private Map<Integer, Bin> binMap = loadingScreen.binMap;
    private ArrayList<Route> routeArrayList = loadingScreen.routeArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_assign_route);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map3);
        mapFragment.getMapAsync(this);

        Button assignRouteBtn = findViewById(R.id.assignRouteBtn);

        assignRouteBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                confirmRouteAssign();
            }
        });

        createAndPopulateDistrictsSpinner();
        createAndPopulateDriversSpinner();
    }

    private void confirmRouteAssign() {
        //establish connection to realtime database and get reference to it
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        String routeIndex = String.valueOf(routeArrayList.indexOf(routeToBeAssigned));
        mDatabase.child("route").child(routeIndex).setValue(routeToBeAssigned)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        showSuccessAlert();
                        routeArrayList.set(Integer.valueOf(routeIndex), routeToBeAssigned);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        showFailAlert();
                    }
                });
    }

    private void showSuccessAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("The route was successfully assigned!");
        alert.setTitle("Success");
        alert.show();

        //remove the route textview
    }

    private void showFailAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("There was an error! Please try again");
        alert.setTitle("Failure");
    }

    private void createAndPopulateDriversSpinner() {
        ArrayList<String> driversSpinnerArray = new ArrayList<String>();

        for(Integer driverId: ManagerLogin.currentUser.getDriverIds()) {
            Driver driver = loadingScreen.driverMap.get(driverId);
            driversSpinnerArray.add(driver.getName() + "\nID:" + driver.getId());
        }

        ArrayAdapter<String> driversArrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_dropdown_item,
                driversSpinnerArray);

        Spinner driversSpinner = findViewById(R.id.DriversSpinner);
        driversSpinner.setAdapter(driversArrayAdapter);

        driversSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String driver =(String)parent.getSelectedItem();
                getDriverToAssign(driver);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                return;
            }
        });
    }

    private void createAndPopulateDistrictsSpinner() {
        Spinner districtsSpinner = findViewById(R.id.DistrictsSpinner);

        ArrayList<String> districtsSpinnerArray = new ArrayList<String>();

        for(Map.Entry<Integer, String> entry : districtMap.entrySet()) {
            districtsSpinnerArray.add(entry.getValue() + "\nID:"+ entry.getKey());
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_dropdown_item,
                districtsSpinnerArray);

        districtsSpinner.setAdapter(spinnerArrayAdapter);
        districtsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String district =(String)parent.getSelectedItem();
                getDistrictToAssign(district);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                return;
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    private void getDistrictToAssign(String districtAndId) {
        int idStartIndex = districtAndId.indexOf(":");
        int districtId = Integer.parseInt(
                districtAndId.substring(idStartIndex+1)
        );
        districtToBeAssigned = new District(districtId, districtMap.get(districtId));

        // We  wanna get the route associated with this district (one to one).
        // The route will give us bin IDs and the we wanna move the camera to the first
        // LatLng of the first bin
        for(Route route: routeArrayList) {
            if(route.getDistrictId() == districtToBeAssigned.getId()) {
                routeToBeAssigned = route;
                break;
            }
        }
        // Get the first bin and move camera to it
        Bin binToMoveTo = binMap.get(routeToBeAssigned.getOrder().get(0));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(binToMoveTo.getCoords(), 15));
    }

    private void getDriverToAssign(String driverNameAndId) {
        // Need to get the driver object
        Integer idStartIndex = driverNameAndId.indexOf(":");
        Integer driverId = Integer.parseInt(driverNameAndId.substring(idStartIndex+1));
        Driver selectedDriver = loadingScreen.driverMap.get(driverId);
        driverToBeAssigned = selectedDriver;
        routeToBeAssigned.setDriverId(selectedDriver.getId());
    }
}
