package com.example.fydp;

import org.json.JSONObject;

public interface VolleyListener {
    public void directionsApiRequestFinished(boolean responseArrived, JSONObject response, Bin binToUpdate, Bin destinationBin);
    public void distanceApiRequestFinished(boolean responseArrived, JSONObject response, Bin binToUpdate, Bin destinationBin);
}
