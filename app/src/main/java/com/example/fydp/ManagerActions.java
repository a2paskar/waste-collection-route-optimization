package com.example.fydp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class ManagerActions extends AppCompatActivity {

    private List<Driver> driverList = loadingScreen.driverArrayList;
    private User currentUser = ManagerLogin.currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_actions);

        Button addNewDriverBtn = findViewById(R.id.addNewDriverBtn);
        Button assignAndOptimizeRouteBtn = findViewById(R.id.AssignRouteBtn);
        Button viewRoutesBtn = findViewById(R.id.viewRoutesBtn);

        assignAndOptimizeRouteBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Intent i = new Intent(ManagerActions.this, ManagerAssignRoute.class);
                startActivity(i);
            }
        });

        addNewDriverBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                addNewDriver();
            }
        });

        viewRoutesBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent i = new Intent(ManagerActions.this, ManagerViewRoutes.class);
                startActivity(i);
            }
        });
    }

    public void addNewDriver() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        final EditText edittext = new EditText(this);
        alert.setMessage("Please enter the driver's name:");
        alert.setTitle("Add New Driver");

        alert.setView(edittext);

        alert.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String driverName = edittext.getText().toString();
                addNewDriverToDB(driverName);
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                return;
            }
        });

        alert.show();
    }

    private void showSuccessAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("The route was successfully assigned!");
        alert.setTitle("Success");
        alert.show();

        //remove the route textview
    }

    private void showFailAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("There was an error! Please try again");
        alert.setTitle("Failure");
    }

    private void addNewDriverToDB(String driverName) {
        Driver newDriver = new Driver((driverList.size()+1), currentUser.getId(), driverName);
        //establish connection to realtime database and get reference to it
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        String newDriverIndex = String.valueOf(driverList.size());
        mDatabase.child("driver").child(newDriverIndex).setValue(newDriver)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        loadingScreen.driverMap.put(newDriver.getId(), newDriver);
                        loadingScreen.driverArrayList.add(newDriver);
                        List<Integer> driverIds = ManagerLogin.currentUser.getDriverIds();
                        //Add the new driver into the managers driver list (locally)
                        driverIds.add(newDriver.getId());
                        ManagerLogin.currentUser.setDriverIds(driverIds);
                        ManagerLogin.currentManager.setDrivers(driverIds);
                        //Update the DB manager's driver list to include the new driver as well!
                        assignNewDriverToManager(newDriver.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        showFailAlert();
                    }
                });
    }

    private void assignNewDriverToManager(Integer newDriverId) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        Integer managerIndex = 0;
        for(Manager m: loadingScreen.managerArrayList) {
            if(m.getId() == currentUser.getId()) {
                break;
            }
            managerIndex++;
        }
        Integer finalManagerIndex = managerIndex;
        mDatabase.child("manager").child(String.valueOf(managerIndex)).child("drivers")
                .setValue(ManagerLogin.currentUser.getDriverIds())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        showSuccessAlert();
                        loadingScreen.managerArrayList.set(
                                finalManagerIndex,
                                ManagerLogin.currentManager);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        showFailAlert();
                    }
                });
    }

}
