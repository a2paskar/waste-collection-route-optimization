package com.example.fydp;

import java.util.List;

public class Manager {
    private Integer id;
    private String name;
    private List<Integer> drivers;

    public Manager(Integer id , String name, List<Integer> drivers) {
        this.id = id;
        this.name = name;
        this.drivers = drivers;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<Integer> drivers) {
        this.drivers = drivers;
    }
}
