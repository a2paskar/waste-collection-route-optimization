package com.example.fydp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DriverRouteActions extends AppCompatActivity {

    public static User currentUser = DriverLogin.currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_route_actions);

        TextView greeting = findViewById(R.id.driverGreeting);
        greeting.setText("Hello " + currentUser.getName() + "! What would you like to do?");

        Button viewRoutesBtn = findViewById(R.id.viewRoutesBtn);
        Button completeRouteBtn = findViewById(R.id.completeRouteBtn);

        viewRoutesBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent i = new Intent(DriverRouteActions.this, DriverViewRoutes.class);
                startActivity(i);
            }
        });

        completeRouteBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(DriverRouteActions.this, DriverCompleteRoute.class);
                startActivity(i);
            }
        });


    }
}
