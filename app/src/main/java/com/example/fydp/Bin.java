package com.example.fydp;

import com.google.android.gms.maps.model.LatLng;
import java.util.HashMap;
import java.util.Map;

public class Bin {

    private Integer id;
    private Integer districtId;
    private Double capacity;
    private LatLng coords;
    private Map<Bin, Double> distanceToBin;
    private Map<Bin, Double> timeToBin;
    private Map<Bin, Integer> leftTurnsToBin;
    private Map<Bin, Double> normalizedDistanceTo;
    private Map<Bin, Double> normalizedTimeTo;
    private Map<Bin, Double> normalizedLeftTurnsTo;

    public Bin(Double capacity, Double latitude, Double longitude, Integer id, Integer districtId) {
        this.capacity = capacity;
        this.coords = new LatLng(latitude, longitude);
        this.distanceToBin = new HashMap<>();
        this.timeToBin = new HashMap<>();
        this.leftTurnsToBin = new HashMap<>();
        this.normalizedDistanceTo = new HashMap<>();
        this.normalizedTimeTo = new HashMap<>();
        this.normalizedLeftTurnsTo = new HashMap<>();
        this.id = id;
        this.districtId = districtId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public Double getNormalizedDistanceTo(Bin bin) {
        return this.normalizedDistanceTo.get(bin);
    }

    public void setNormalizedDistanceTo(Bin bin, Double distance) {
        this.normalizedDistanceTo.put(bin, distance);
    }

    public Double getNormalizedTimeTo(Bin bin) {
        return this.normalizedTimeTo.get(bin);
    }

    public void setNormalizedTimeTo(Bin bin, Double time) {
        this.normalizedTimeTo.put(bin, time);
    }

    public Double getNormalizedLeftTurnsTo(Bin bin) {
        return this.normalizedLeftTurnsTo.get(bin);
    }

    public void setNormalizedLeftTurnsTo(Bin bin, Double turns) {
        this.normalizedDistanceTo.put(bin, turns);
    }


    public void setLeftTurnsToBin(Bin bin, int numOfLeftTurns) {
        try {
            this.leftTurnsToBin.put(bin, numOfLeftTurns);
        } catch (Exception ex) {
            throw ex;
        }
    }


    public void setDistanceTo(Bin bin, Double distance) {
        try {
            this.distanceToBin.put(bin, distance);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void setTimeTo(Bin bin, Double time) {
        try {
            this.timeToBin.put(bin, time);
        } catch (Exception ex) {
            throw ex;
        }
    }



    public Double getTimeTo(Bin bin) {
        return this.timeToBin.get(bin);
    }

    public Double getDistanceTo(Bin bin) {
        return this.distanceToBin.get(bin);
    }

    public int getLeftTurnsTo(Bin bin) {
        return this.leftTurnsToBin.get(bin);
    }

    public Double getCostTo(Bin bin ){
        return (getTimeTo(bin)/getDistanceTo(bin))*getLeftTurnsTo(bin);
    }

    public LatLng getCoords() {
        return this.coords;
    }

    public Double getCapacity() {
        return this.capacity;
    }

}
