package com.example.fydp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.TextViewCompat;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class DriverCompleteRoute extends AppCompatActivity {
    private User currentUser = DriverLogin.currentUser;
    private List<Route> userRoutes = new ArrayList<>();
    private Route selectedRoute;
    private Integer textViewIdCounter = 1;
    private ArrayList<Route> routeArrayList = loadingScreen.routeArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_complete_route);

        LinearLayout layout = findViewById(R.id.CompleteRouteLayout);

        addInformationTextView(layout);

        //Get all of the current user's incomplete routes
        for(Route route: loadingScreen.routeArrayList) {
            if(route.getDriverId() == currentUser.getId() && route.getStatus().equals("incomplete")) {
                userRoutes.add(route);
            }
        }

        //Add a textview for each route
        for(Route route: userRoutes) {
            addRouteTextView(layout, route);
        }
    }

    private void addRouteTextView(LinearLayout layout, Route route) {
        TextView routeTextView = new TextView(this);
        routeTextView.setText("Route Location: " + loadingScreen.districtMap.get(route.getDistrictId())
                + "\nRoute ID: " + route.getId());
        routeTextView.setPaintFlags(routeTextView.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        TextViewCompat.setTextAppearance(routeTextView, android.R.style.TextAppearance_DeviceDefault_Large);
        routeTextView.setId(textViewIdCounter);
        textViewIdCounter++;
        routeTextView.setGravity(Gravity.CENTER);
        routeTextView.setPadding(0,20,0,20);
        layout.addView(routeTextView);

        routeTextView.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                markRouteAsComplete(route, routeTextView, layout);
            }
        });
    }

    private void addInformationTextView(LinearLayout layout) {
        //set the properties for button
        TextView informationText = new TextView(this);
        //btnTag.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT));
        informationText.setText("All of your incomplete routes are listed below. To mark a route " +
                "as complete, simply press on the route and then hit confirm.");
        TextViewCompat.setTextAppearance(informationText, android.R.style.TextAppearance_DeviceDefault_Large);
        informationText.setId((int)1);
        informationText.setGravity(Gravity.CENTER);
        informationText.setPadding(40,50,40,40);
        layout.addView(informationText);
    }

    private void markRouteAsComplete(Route route, TextView routeTextView, LinearLayout layout) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("Are you sure you want to mark this route as complete?:");
        alert.setTitle("Mark Route As Complete");

        alert.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                updateDb(route, routeTextView, layout);
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                return;
            }
        });

        alert.show();
    }

    private void showSuccessAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("The route was successfully completed. It will now be removed from the list");
        alert.setTitle("Success");
        alert.show();

        //remove the route textview
    }

    private void showFailAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("There was an error! Please try again");
        alert.setTitle("Failure");
    }

    private void updateDb(Route route, TextView routeTextView, LinearLayout layout) {
        //establish connection to realtime database and get reference to it
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        String routeIndex = String.valueOf(routeArrayList.indexOf(route));
        mDatabase.child("route").child(routeIndex).child("status").setValue("complete")
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        showSuccessAlert();
                        //If update is successful then update local variables as well
                        route.setStatus("complete");
                        routeArrayList.set(Integer.valueOf(routeIndex), route);
                        //Remove from list
                        layout.removeView(routeTextView);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        showFailAlert();
                    }
                });
    }

}
