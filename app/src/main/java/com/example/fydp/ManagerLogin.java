package com.example.fydp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class ManagerLogin extends AppCompatActivity {

    public static ArrayList<Manager> managerArrayList = loadingScreen.managerArrayList;
    public static User currentUser = new User();
    public static Manager currentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_login);

        Button confirmManagerIdBtn = findViewById(R.id.confirmManagerIdBtn);
        Button backButton = findViewById(R.id.backToUserIdentityButton2);
        EditText managerId = findViewById(R.id.managerIdEditText);

        confirmManagerIdBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Integer id = Integer.valueOf(String.valueOf(managerId.getText()));
                Boolean managerFound = false;
                for(Manager d: managerArrayList) {
                    if(d.getId() == id) {
                        currentUser.setId(id);
                        currentUser.setRole("manager");
                        currentUser.setName(d.getName());
                        currentUser.setDriverIds(d.getDrivers());
                        currentManager = new Manager(d.getId(), d.getName(), d.getDrivers());
                        managerFound = true;
                        break;
                    }
                }
                if(!managerFound) {
                    Intent i = new Intent(ManagerLogin.this, UserNotFound.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(ManagerLogin.this, ManagerActions.class);
                    startActivity(i);
                }
            }
        });
        backButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent i = new Intent(ManagerLogin.this, IdentifyUser.class);
                startActivity(i);
            }
        });

    }
}
