package com.example.fydp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class IdentifyUser extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_identify_user);

        Button driverBtn = (Button) findViewById(R.id.driverButton);
        Button managerBtn = (Button) findViewById(R.id.managerButton);

        driverBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent i = new Intent(IdentifyUser.this, DriverLogin.class);
                startActivity(i);
            }
        });

        managerBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent i = new Intent(IdentifyUser.this, ManagerLogin.class);
                startActivity(i);
            }
        });
    }

}
