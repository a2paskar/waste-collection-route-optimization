package com.example.fydp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class User {
    private String role;
    private Integer id;
    private String name;
    private List<Integer> driverIds;

    public User() {
        this.role = null;
        this.id = null;
        this.name = null;
        this.driverIds = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Integer> getDriverIds() {
        return driverIds;
    }

    public void setDriverIds(List<Integer> driverIds) {
        this.driverIds = driverIds;
    }
}
