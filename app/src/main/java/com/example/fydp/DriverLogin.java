package com.example.fydp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class DriverLogin extends AppCompatActivity {

    public static ArrayList<Driver> driverArrayList = loadingScreen.driverArrayList;
    public static User currentUser = new User();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_login);

        Button confirmDriverIdBtn = findViewById(R.id.confirmDriverIdBtn);
        Button backButton = findViewById(R.id.backToUserIdentityButton);
        EditText driverId = findViewById(R.id.driverIdEditText);

        confirmDriverIdBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Integer id = Integer.valueOf(String.valueOf(driverId.getText()));
                Boolean driverFound = false;
                for(Driver d: driverArrayList) {
                    if(d.getId() == id) {
                        currentUser.setId(id);
                        currentUser.setRole("driver");
                        currentUser.setName(d.getName());
                        driverFound = true;
                        break;
                    }
                }
                if(!driverFound) {
                    Intent i = new Intent(DriverLogin.this, UserNotFound.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(DriverLogin.this, DriverRouteActions.class);
                    startActivity(i);
                }
            }
        });
        backButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent i = new Intent(DriverLogin.this, IdentifyUser.class);
                startActivity(i);
            }
        });

    }
}
