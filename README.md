# Waste Collection Route Optimization ~ 4th Year Capstone Project

Waste collection route optimization app using Android Studio (JavaScript) + Google Maps API + Cloud Storage

Special thanks to Osama Rai, Yonna Worku, Henry Odunze for their contributions to the project.

<ul>
    <li><strong>GENE-4040-Design-Report.pdf</strong> for Capstone Project Report which describes problem statement and solution implementation details</li>
    <li><strong>GENE-404-DPR4.pptx</strong> for Capstone Project Final Presentation for project overview </li>
    <li><strong>FYDP_Poster.pdf</strong> for Capstone Project Poster</li>
    <li><strong>demo.mp4</strong> for software app demo</li>
</ul>

